Simple descriptor testing framework, based on OpenCV v2.4.13

# Building and execution
You'll need CMake 3.5 or a newer one

## Building
CLion would parse CMakeLists.txt and create run configurations

for manual build use:
```bash
cmake .
cmake --build . --target descriptor_test -- -j 4
```
## Execution
binary is generated in project's *./bin* directory

update CLion's "descriptor_test" configuration passing a testing image as an argument
```
../resources/image.png
```
or
run executable itself with an argument
```bash
./bin/descriptor_test resources/image.png
```