#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <memory>

#include "Transformation.h"

using namespace std;
using namespace cv;

class TestContext {
    using DescriptorExtractorPtr = Ptr<DescriptorExtractor>;
    using FeatureDetectorPtr = Ptr<FeatureDetector>;

private:
    static constexpr int MIN_HESSIAN = 400;

    string description;

    FeatureDetectorPtr featureDetector;

    DescriptorExtractorPtr descriptorExtractorImpl;
    TransformationPtr transformationImpl;

    Mat initialImage;
    Mat transformedImage;

    Mat initialDescriptors;
    Mat transformedDescriptors;

    vector<KeyPoint> initialKeyPoints;
    vector<KeyPoint> transformedKeyPoints;

    vector<float> mistakes;
    float averageMistake;

    float closestDescriptors;
    float mostDistantDescriptors;

public:
    TestContext(const string& testDescription, const DescriptorExtractorPtr _extractorImpl,
                const TransformationPtr _transformationImpl, const Mat _initialImage,
                const FeatureDetectorPtr _featureDetector = makePtr<SurfFeatureDetector>(MIN_HESSIAN))
            : description(testDescription)
              , descriptorExtractorImpl(_extractorImpl)
              , transformationImpl(_transformationImpl)
              , initialImage(_initialImage)
              , featureDetector(_featureDetector)
    {
        // extract points of interest
        featureDetector->detect(initialImage, initialKeyPoints);

        // apply transformation to image
        transformedImage = Mat::zeros(initialImage.rows, initialImage.cols, initialImage.type());
        transformationImpl->rotateMat(initialImage, transformedImage);

        // apply transformation to points
        transformationImpl->rotatePoints(initialKeyPoints, transformedKeyPoints);
    }

    const string getDescription() const {
        return description;
    }

    float executeTest() {
        descriptorExtractorImpl->compute(initialImage, initialKeyPoints, initialDescriptors);
        descriptorExtractorImpl->compute(transformedImage, transformedKeyPoints, transformedDescriptors);

        mistakes.clear();

        closestDescriptors = numeric_limits<float>::max();
        mostDistantDescriptors = 0.0f;

        for (int i = 0; i < initialDescriptors.rows; ++i) {
            auto row1 = initialDescriptors.row(i);
            auto row2 = transformedDescriptors.row(i);
            float descriptorMistake = 0.0f;
            for (int j = 0; j < descriptorExtractorImpl->descriptorSize(); ++j) {
                descriptorMistake += abs(row1.at<float>(j) - row2.at<float>(j));
            }

            closestDescriptors = min(closestDescriptors, descriptorMistake);
            mostDistantDescriptors = max(mostDistantDescriptors, descriptorMistake);

            mistakes.push_back(descriptorMistake);
        }

        return averageMistake = std::accumulate(mistakes.begin(), mistakes.end(), 0.0f, std::plus<float>()) / mistakes.size();
    }

    const Mat getTransformedImage() const {
        return transformedImage;
    }

    const vector<KeyPoint>& getInitialKeyPoints() const {
        return initialKeyPoints;
    }

    const vector<KeyPoint>& getTransformedKeyPoints() const {
        return transformedKeyPoints;
    }
};

constexpr int TestContext::MIN_HESSIAN;
