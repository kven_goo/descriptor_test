#pragma once

#include <opencv2/opencv.hpp>
#include <memory>

using namespace std;
using namespace cv;

class Transformation {
public:
    virtual void rotatePoints(const vector<KeyPoint>, CV_OUT vector<KeyPoint>&) const = 0;
    virtual void rotateMat(const Mat, CV_OUT Mat&) const = 0;
    virtual const string getDescription() const = 0;
};

using TransformationPtr = Ptr<Transformation>;

class PassThroughTransformation : public Transformation {
public:
    virtual void rotatePoints(const vector<KeyPoint> vector1, vector<KeyPoint> &vector2) const override {
        vector2 = vector1;
    }

    virtual void rotateMat(const Mat mat, Mat& mat1) const override {
        mat1 = mat;
    }

    virtual const string getDescription() const override {
        return "Simple pass-through transform";
    }
};
