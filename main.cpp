#include <iostream>
#include <opencv2/opencv.hpp>

#include "src/TestContext.h"

using namespace std;
using namespace cv;

class CustomAffineTransform : public Transformation {
private:
    Mat affineTransform;
public:
    virtual const string getDescription() const override {
        return "Affine transformation";
    }

    CustomAffineTransform(const Mat affineTransform)
            : affineTransform(affineTransform) {}

    virtual void rotatePoints(const vector<KeyPoint> vector1, CV_OUT vector<KeyPoint>& vector2) const override {
        Mat_<float> transform = affineTransform;
        vector2.clear();
        for (const auto &point : vector1) {
            Mat asMat(Point3_<float>(point.pt.x, point.pt.y, 1.0f));
            auto transformed = (Mat)(transform * asMat);
            KeyPoint newPoint(transformed.at<float>(0), transformed.at<float>(1), point.size);
            vector2.push_back(newPoint);
        }
    }

    virtual void rotateMat(const Mat mat, Mat& mat1) const override {
        warpAffine(mat, mat1, affineTransform, mat1.size());
    }
};

string WINDOW = "named_window_1";

int main(int argc, char **argv) {
    auto src = imread(argv[1], IMREAD_GRAYSCALE);

    Point2f srcTri[3]{
            Point2f(0, 0),
            Point2f(src.cols - 1, 0),
            Point2f(0, src.rows - 1)
    };
    Point2f dstTri[3]{
            Point2f(src.cols * 0.0, src.rows * 0.33),
            Point2f(src.cols * 0.85, src.rows * 0.25),
            Point2f(src.cols * 0.15, src.rows * 0.7)
    };

    // Get the Affine Transform
    auto warp_mat = getAffineTransform(srcTri, dstTri);
    string testName = "test test";
    TestContext context(testName, DescriptorExtractor::create("SIFT"),
                        makePtr<CustomAffineTransform>(warp_mat), src);

    cout << context.executeTest() << endl;

    TestContext context1(testName, DescriptorExtractor::create("SIFT"),
                        makePtr<PassThroughTransformation>(), src);

    cout << context1.executeTest() << endl;

    Mat keyPointsMat;

    drawKeypoints(src, context.getInitialKeyPoints(), keyPointsMat, Scalar::all(-1), DrawMatchesFlags::DEFAULT);

    namedWindow(WINDOW, CV_WINDOW_KEEPRATIO);
    imshow(WINDOW, keyPointsMat);

    drawKeypoints(context.getTransformedImage(), context.getTransformedKeyPoints(), keyPointsMat, Scalar::all(-1), DrawMatchesFlags::DEFAULT);
    imshow(WINDOW, keyPointsMat);

    return 0;
}